"use strict";


document.querySelector(".tabs").addEventListener("click", (event) => {
    document.querySelector(".active").classList.remove("active")
    event.target.classList.add("active")

    document.querySelector(".show").classList.remove("show")
    // console.log(event.target.getAttribute("data-tab"))
    const dataAtr = event.target.getAttribute("data-tab");
    document.querySelector(`.tabs-content[data-tab=${dataAtr}]`).classList.add("show");
})


// <==========================OUR AMAZING WORK===========================>

let ps = document.querySelectorAll('.portfolio-show');
let parentLi = document.querySelector(".great-work");
parentLi.addEventListener("click", (event) => {
    document.querySelector(".great-active").classList.remove("great-active")
    event.target.classList.add("great-active")
    let currentData = event.target.getAttribute("data-great")
    console.log(currentData)
    for (let i = 0; i < ps.length; i++) {
        if (ps[i].getAttribute("data-great") === currentData) {
            ps[i].classList.add("portfolio-show");
        } else {
            ps[i].classList.remove("portfolio-show");
        }
        if (currentData === "all") {
            ps[i].classList.add("portfolio-show");
        }
    }
})

const newItems = [
    {
        url: "./img/graphicDesign/graphic-design4.jpg",
        data: "graphicDesign",
        text: "GraphicDesign"
    },
    {
        url: "./img/graphicDesign/graphic-design5.jpg",
        data: "graphicDesign",
        text: "GraphicDesign"
    },
    {
        url: "./img/graphicDesign/graphic-design6.jpg",
        data: "graphicDesign",
        text: "GraphicDesign"
    },
    {
        url: "./img/webDesign/web-design4.jpg",
        data: "webDesign",
        text: "WebDesign"
    },
    {
        url: "./img/webDesign/web-design5.jpg",
        data: "webDesign",
        text: "WebDesign"
    },
    {
        url: "./img/webDesign/web-design6.jpg",
        data: "webDesign",
        text: "WebDesign"
    },
    {
        url: "./img/landingPage/landing-page4.jpg",
        data: "landingPages",
        text: "LandingPages"
    },
    {
        url: "./img/landingPage/landing-page5.jpg",
        data: "landingPages",
        text: "LandingPages"
    },
    {
        url: "./img/landingPage/landing-page6.jpg",
        data: "landingPages",
        text: "LandingPages"
    },
    {
        url: "./img/wordpress/wordpress4.jpg",
        data: "wordpress",
        text: "Wordpress"
    },
    {
        url: "./img/wordpress/wordpress5.jpg",
        data: "wordpress",
        text: "Wordpress"
    },
    {
        url: "./img/wordpress/wordpress6.jpg",
        data: "wordpress",
        text: "Wordpress"

    }

]
const btnShowMore = document.querySelector(".showMore")
const parentImg = document.querySelector(".portfolio-list")
btnShowMore.addEventListener("click", (event) => {

    event.target.style.display = "none";
    newItems.forEach((item) => {

        const li = `

        <li data-great="${item.data}" class="portfolio-list-item portfolio-show">
                <img
                        class="portfolio-image"
                        src="${item.url}"
                        width="285"
                        height="211"
                        alt="img-1"
                />
                <div class="hidden-block">
                    <div class="hidden-portfolio-image">
                        <div class="chain"></div>
                        <div class="square"></div>
                    </div>
                    <p class="title-hidden-block">
                        <span>creative design</span>
                    </p>

                    <p class="text-hidden-block">
                        ${item.text}
                    </p>
                </div>
            </li>

        `
        parentImg.innerHTML += li

    })

})


// <===============================RESPONSE====================================>

document.querySelector(".response-tabs").addEventListener("click", (event) => {

    document.querySelector(".response-show").classList.remove("response-show")
    const dataAtrResp = event.target.closest("[data-resp]").getAttribute("data-resp");
    document.querySelector(`.response-tabs-content[data-resp=${dataAtrResp}]`).classList.add("response-show");

})